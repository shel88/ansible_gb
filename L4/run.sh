#!/usr/bin/env bash


ansible-playbook second_task.yml \
       	-i inventories/dev/hosts \
       	--ask-vault-pass
        --list-tags 
	"$@"

